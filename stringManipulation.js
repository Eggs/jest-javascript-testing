const capitalise = (string) => string.toUpperCase();

const reverse = (string) => string.split('').reverse().join('');

module.exports = {
  capitalise,

  reverse,
};
