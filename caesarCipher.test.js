/* eslint-disable no-undef */
const { Caesar } = require('./caesarCipher');

test('checks caesar cipher shifts correctly', () => {
  expect(Caesar.encrypt('This is a test', 1)).toEqual('Uijt jt b uftu');
});
