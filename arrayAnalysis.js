const ArrayAnalysis = (() => {

    const average = (array) => {
        const sum = array.reduce((prev, curr) => {
            return prev + curr
        });

        return sum / array.length;
    };

    const min = (array) => {
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/apply#using_apply_and_built-in_functions
        return Math.min.apply(Math, array);
    };

    const max = (array) => {
        return Math.max.apply(Math, array);
    };

    const length = (array) => {
        return array.length;
    };

    const analyse = (array) => {
        return {
            "average": average(array),
            "min": min(array),
            max: 4,
            length: 4,
        }
    };
    return {
        analyse,
    }
})();

module.exports = {
  ArrayAnalysis,
};
