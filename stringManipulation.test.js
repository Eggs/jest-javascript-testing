const { capitalise, reverse } = require('./stringManipulation');

test('checks if string was capitalised', () => {
  expect(capitalise('string')).toEqual('STRING');
});

test('checks if string was reversed', () => {
  expect(reverse('string')).toEqual('gnirts');
});
