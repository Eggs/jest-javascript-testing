/* eslint-disable no-undef */
const { Calculator } = require('./calculator');

test('adds one number to another', () => {
  expect(Calculator.add(1, 2)).toBe(3);
});

test('subtracts one number from another', () => {
  expect(Calculator.sub(2, 1)).toBe(1);
});

test('multiplies one number to another', () => {
  expect(Calculator.mul(2, 2)).toBe(4);
});

test('divides one number from another', () => {
  expect(Calculator.div(2, 2)).toBe(1);
});
