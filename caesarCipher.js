const Caesar = (() => {
  const alphabet = ['a', 'b', 'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j', 'k', 'l',
    'm', 'n', 'o', 'p', 'q', 'r',
    's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
  const alphabetUpper = alphabet.map((character) => character.toUpperCase());

  const encrypt = (string, shiftAmount) => {
    const splitString = string.split('');
    const newString = [];
    let count = 0;
    const spaces = [];

    splitString.forEach((letter) => {
      if (letter === ' ') {
        spaces.push(count);
      }
      count += 1;
      alphabet.forEach((character) => {
        if (letter === character) {
          newString.push(alphabet[
            alphabet.indexOf(character)
            + shiftAmount
          ]);
        } else if (letter === alphabetUpper[alphabet.indexOf(character)]) {
          newString.push(alphabetUpper[
            alphabet.indexOf(character)
            + shiftAmount
          ]);
        }
      });
    });

    spaces.forEach((num) => {
      newString.splice(num, 0, ' ');
    });

    return newString.join('');
  };
  return {
    encrypt,
  };
})();

module.exports = {
  Caesar,
};
