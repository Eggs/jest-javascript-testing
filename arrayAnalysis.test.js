const { ArrayAnalysis } = require('./arrayAnalysis');

const array = ArrayAnalysis.analyse([1,2,3,4]);

test('Object has correct average', () => {
  expect(array.average).toBe(2.5);
});

test('Object has correct min', () => {
  expect(array.min).toBe(1);
});

test('Object has correct max', () => {
  expect(array.max).toBe(4);
});

test('Object has correct length', () => {
  expect(array.length).toBe(4);
});
